package edu.sjsu.android.zoodirectory;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    // new variables to hold values that will be passed to mainActivity
    String array1[]; // array for animals
    String array2[]; // array for description
    int img[];
    Context ct;

    public MyAdapter(Context context, String animals[], String description[], int images[]) {
        ct = context;
        array1 = animals;
        array2 = description;
        img = images;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(ct);
        View view = inflater.inflate(R.layout.row_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.myText.setText(array1[position]);
        holder.myImage.setImageResource(img[position]);

        holder.mainLayout.setOnClickListener(v -> {
            Intent intent = new Intent(ct, DetailsActivity.class);
            intent.putExtra("array1", array1[position]);
            intent.putExtra("array2", array2[position]);
            intent.putExtra("mainImageView", img[position]);
            ct.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return array1.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView myText;
        ImageView myImage;
        ConstraintLayout mainLayout;

        public MyViewHolder (View itemView) {
            super(itemView);
            myText = itemView.findViewById(R.id.myText1);
            myImage = itemView.findViewById(R.id.myImageView);
            mainLayout = itemView.findViewById(R.id.mainLayout);
        }
    }
}

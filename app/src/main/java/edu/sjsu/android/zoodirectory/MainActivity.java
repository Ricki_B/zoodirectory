package edu.sjsu.android.zoodirectory;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    String animals[];
    String description[];
    int images[] = {R.drawable.cow, R.drawable.pig,
                    R.drawable.panda, R.drawable.giraffe, R.drawable.tiger};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.my_recycler_view);


        // storing string variables into array
        animals = getResources().getStringArray(R.array.animals);
        description = getResources().getStringArray(R.array.description);

        // initialize MyAdapter class and pass parameters
        MyAdapter myAdapter = new MyAdapter(this, animals, description, images);

        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public boolean onCreateOptionsMenu (Menu menu) {
        // inflate the menu, this adds items to the action bar if it is present
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }


    public boolean onOptionsItemSelected (MenuItem item) {
        switch (item.getItemId()) {
            case R.id.information:
                    zooInformation();
                return true;
            case R.id.uninstall:
                    uninstall();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void zooInformation() {
        Intent intent = new Intent(MainActivity.this, ZooInformationActivity.class);
        startActivity(intent);
    }

    public void uninstall() {
        Intent intent = new Intent(Intent.ACTION_DELETE);
        startActivity(intent);
    }
}
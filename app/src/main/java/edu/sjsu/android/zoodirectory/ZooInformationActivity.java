package edu.sjsu.android.zoodirectory;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class ZooInformationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoo_information);
        //Button number = (Button) findViewById(R.id.phone_button);
    }

    public void onClick(View view) {
        if (view.getId() == R.id.phone_button) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            startActivity(intent);
        }

    }

    public boolean onCreateOptionsMenu (Menu menu) {
        // inflate the menu, this adds items to the action bar if it is present
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }


    public boolean onOptionsItemSelected (MenuItem item) {
        switch (item.getItemId()) {
            case R.id.information:
                zooInformation();
                return true;
            case R.id.uninstall:
                uninstall();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void zooInformation() {
        Intent intent = new Intent(ZooInformationActivity.this, ZooInformationActivity.class);
        startActivity(intent);
    }

    public void uninstall() {
        Intent intent = new Intent(Intent.ACTION_DELETE);
        startActivity(intent);
    }
}
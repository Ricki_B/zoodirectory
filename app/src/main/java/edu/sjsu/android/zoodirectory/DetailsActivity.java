package edu.sjsu.android.zoodirectory;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class DetailsActivity extends AppCompatActivity {

    // create objects for imageView and 2TextViews
    ImageView mainImageView;
    TextView animal_type;
    TextView description;

    String animal;
    String animal_description;
    int myImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);


        mainImageView = findViewById(R.id.mainImageView);
        animal_type = findViewById(R.id.title);
        description = findViewById(R.id.description);

        getData();
        setData();
    }

    private void getData() {
        if(getIntent().hasExtra("mainImageView") && getIntent().hasExtra("array1") &&
            getIntent().hasExtra("array2")) {

            myImage = getIntent().getIntExtra("mainImageView", 1);
            animal = getIntent().getStringExtra("array1");
            animal_description = getIntent().getStringExtra("array2");

        } else {
            Toast.makeText(this, "Data Unsuccessful", Toast.LENGTH_SHORT).show();
        }
    }

    private void setData() {
        mainImageView.setImageResource(myImage);
        animal_type.setText(animal);
        description.setText(animal_description);
    }

    public boolean onCreateOptionsMenu (Menu menu) {
        // inflate the menu, this adds items to the action bar if it is present
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }


    public boolean onOptionsItemSelected (MenuItem item) {
        switch (item.getItemId()) {
            case R.id.information:
                zooInformation();
                return true;
            case R.id.uninstall:
                uninstall();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void zooInformation() {
        Intent intent = new Intent(DetailsActivity.this, ZooInformationActivity.class);
        startActivity(intent);
    }
    public void uninstall() {
        Intent intent = new Intent(Intent.ACTION_DELETE);
        startActivity(intent);
    }
}